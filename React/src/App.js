import React, { useState } from 'react';
import ExpenseList from './components/Expenses/ExpenseList';
import NewExpense from './components/NewExpense/NewExpense'
const  App = () => {
  
  const [expenseData, setExpenseData] = useState(
    [ 
      {
        date: new Date(2021, 3, 14),
        description: 'Taxi expenses',
        amount: 45.60
      },
    ]
  )


  const onNewExpenseAdded = (expense) =>{   
    const newExpenseData = [expense, ...expenseData ]
    setExpenseData(newExpenseData)
  }
  return (
    <div>
      <NewExpense onNewExpenseAdded={onNewExpenseAdded}/>
      <ExpenseList data = {expenseData} />
    </div>
  )
}
export default App;
