import React from 'react';
import { Fragment } from "react";
import './DateElement.css';
import Card from './Card';

const DateElement = ({date}) =>{
    const year = date.getFullYear();
    const month = date.toLocaleString('en-US', { month: 'long' })
    const day = date.toLocaleString('en-US', { day: '2-digit' });
    return (
        <Fragment>
            <Card className= 'expense-date'>
                <div className='expense-date__year'>{year}</div>
                <div className='expense-date__month'>{month}</div>
                <div className='expense-date__day'>{day}</div>
            </Card>
        </Fragment>
    )

}
export default DateElement