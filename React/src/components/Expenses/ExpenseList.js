import React, {useState} from 'react';
import ExpenseItem from './ExpenseItem';
import './ExpenseList.css';
import Card from '../UI/Card';
import ExpensesFilter from './ExpensesFilter';

const ExpenseList = (props) => {   
    const [filterState, setfilterState] = useState('')
    const onFilterInput = (value) =>{
        setfilterState(()=>value);    
    }
    const expenses = filterState =='' ? props.data : props.data.filter(x=>x.date.getFullYear()==filterState)
    console.log(filterState=='')
    return(
        <div>
            <ExpensesFilter onFilterInput = {onFilterInput}/>
            <Card className='expenses'>
                { expenses.map(expense=> <ExpenseItem key = {expense.id} date= {expense.date} description = {expense.description} amount = {expense.amount} />) }               
            </Card>
        </div>
    )
}

export default ExpenseList; 