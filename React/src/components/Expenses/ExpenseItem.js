import React from 'react';
import './ExpenseItem.css';
import DateElement from '../UI/DateElement';
import Card from '../UI/Card';
import {useState} from 'react';

const ExpenseItem = ({date, description, amount}) => {
   
    const [state, setstate] = useState({date, description, amount})
        
    return (  
        <Card className = 'expense-item'>
                <DateElement date = {state.date}/>
                <div className='expense-item__description'>
                    <h2> {state.description}</h2>
                    <div className='expense-item__price'> {state.amount}</div>
                </div>
                
        </Card>
    )
}
export default ExpenseItem;