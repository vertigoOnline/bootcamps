import React from 'react'
import './NewExpense.css';
import ExpenseForm from './ExpenseForm';


const NewExpense = (props) =>{

    const onSaveFormData=(data)=> {
        props.onNewExpenseAdded({...data, id: Math.random()})
    }

    return (
        <div className='new-expense'>
            <ExpenseForm onSaveFormData={onSaveFormData}/>
        </div>
    )
}
export default NewExpense;