import React, {useState} from 'react';
import './ExpenseForm.css';

const ExpenseForm =(props)=>{
    const [formData, setFormData] = useState({
        date: '',
        description: '',
        amount: '',
    })

    const handleFormInput = (e) =>{
        // setFormData({...formData, [e.target.name]:e.target.value })
        //react shedules state update so in theory if used very often - we  can endup using old state. 
        //In this case its better to pass function, instead of object. Then react guarantees update
        setFormData((oldState)=>{
              return {...oldState, [e.target.name]:e.target.value }
        })
    }

    const handleFormSubmit = (e) =>{
        e.preventDefault();
        props.onSaveFormData({...formData, date: new Date(formData.date)})
        setFormData({
            date:   '',
            amount: '',
            description: '',
        });
      
    }

    return( 
        <form onSubmit={e=>handleFormSubmit(e)}>
            <div className='new-expense__controls'>
                    <div className='new-expense__control'>
                        <label>Description</label>
                        <input type='text' name ='description' onChange ={e=>handleFormInput(e)} value={formData.description}/>
                    </div>
                
                    <div className='new-expense__control'>
                        <label>Amount</label>
                        <input type='number' name='amount' min='0.01' step='0.01' onChange ={e=>handleFormInput(e)} value = {formData.amount}/>
                    </div>
                
                    <div className='new-expense__control'>
                        <label>Date</label>
                        <input type='date' name='date' min='2019-01-01' max='2021-05-27' required='true' onChange ={e=>handleFormInput(e)} value ={formData.date}/>
                    </div>
                </div>
                <div className='new-expense__actions'>
                    <button type='submit'>Add expense</button>
                </div>
        </form>
    )
}
export default ExpenseForm;