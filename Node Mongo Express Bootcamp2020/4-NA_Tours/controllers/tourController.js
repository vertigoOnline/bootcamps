// const fs = require('fs');
const Tour = require('./../models/tourModel');

// const filePath = `${__dirname}/../dev-data/data/tours-simple.json`;
// const tours = JSON.parse(fs.readFileSync(filePath));

const createNewTour = async (req, res) => {
  try {
    const newTour = await Tour.create(req.body);
    res.status(201).json({
      status: 'success',
      data: {
        tour: newTour,
      },
    });
  } catch (error) {
    res.status(401).json({
      status: 'error',
      error,
    });
  }

};

const getAllTours = async (req, res) => {
  try {
    const tours = await Tour.find();
    res.status(200).json({
      status: 'success',
      data: {
        tours,
      },
    });
  } catch (error) {
    res.status(404).json({
      status: 'internal error',
      error,
    });
  }
};
const getTourByID = async (req, res) => {
  try {
    // const tour = await Tour.find({
    //   _id: req.params.id,
    // });
    // I can also use findByID. It is healper function
    const tour = await Tour.findById(req.params.id);
    res.status(200).json({
      status: 'success',
      data: tour,
    });
  } catch (error) {
    res.status(404).json({
      status: 'error cant find record',
      error,
    });
  }
};
const updateTour = async (req, res) => {
  try {
    const updatedTour = await Tour.findByIdAndUpdate(req.params.id, req.body, {
      new: true, //   "new" means to return new created object
      runValidators: true, // validate against model schema
    });
    res.status(200).json({
      status: 'success',
      data: {
        updatedTour,
      },
    });
  } catch (error) {
    res.status(404).json({
      status: 'error cant find record',
      error,
    });
  }
};
const deleteTour = async (req, res) => {
  try {
    await Tour.findByIdAndDelete(req.params.id);
    res.status(204).json({
      status: 'success, object removed',
      data: null,
    });
  } catch (error) {
    res.status(401).json({
      status: 'error trying to delete object',
      error,
    });
  }
};
module.exports = {
  getAllTours,
  createNewTour,
  getTourByID,
  updateTour,
  deleteTour,
};