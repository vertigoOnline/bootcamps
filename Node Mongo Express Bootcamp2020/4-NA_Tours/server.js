const mongoose = require('mongoose');
//  dotenv extension reads config file and adds parameters to process.env
//  which we can then access later
const dotenv = require('dotenv');

dotenv.config({
  path: './config.env',
});

const port = process.env.port || 3000;
const app = require('./app');

const DB_URL = process.env.DATABASE_STRING.replace(
  '<password>',
  process.env.DATABASE_PASSWORD,
);

//  Following parameters are used to turn off deprecation warnings
mongoose.connect(DB_URL, {
  useNewUrlParser: true,
  useCreateIndex: true,
  useFindAndModify: false,
  useUnifiedTopology: true,
}).then(() => {
  // console.log(connection.connections);
  console.log('SERVER: Mongo connection established 🌟');
});


// console.log(app.get('env')); //env is set by express but
app.listen(port, () => {
  console.log('SERVER: started, listening on port : ' + port);
  if (process.env.NODE_ENV === 'production') console.log('!!!! in PRODUCTION !!!');
});