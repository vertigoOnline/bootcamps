exports.verifyRequest = (req, res, next) => {
  const tour = req.body
  if (!tour.name || !tour.price) {
    res.status(400).json({
      status: 'bad request',
      message: `tour information is missing in the request{ ${tour} } (check tour name and tour price exists)`
    })
  }
  next()
}

exports.checkID = (req, res, next, value) => {
  if (req.params.id > tours.length) {
    return res.status(404).json({
      status: "error",
      message: "invalid ID"
    })
  } else next()
}

// module.exports = verifyRequest