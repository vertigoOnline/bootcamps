const mongoose = require('mongoose');
const fs = require('fs'); //  To read data from file
const dotenv = require('dotenv'); //  To access env variables
const Tour = require('./../../models/tourModel');

dotenv.config({
  path: `${__dirname}/../../config.env`,
});

const DB_URL = process.env.DATABASE_STRING.replace(
  '<password>',
  process.env.DATABASE_PASSWORD,
);

mongoose.connect(DB_URL, {
  useNewUrlParser: true,
  useCreateIndex: true,
  useFindAndModify: false,
  useUnifiedTopology: true,
}).then(() => {
  console.log('SERVER: Mongo connection established 🌟');
});

const createTours = async (data) => {
  try {
    await Tour.create(data);
    console.log(`Data successfully imported`);
    process.exit();
  } catch (error) {
    console.log(error);
  }
};

const deleteTours = async () => {
  try {
    await Tour.deleteMany();
    console.log('Database cleared');
    process.exit();
  } catch (error) {
    console.log(error);
  }
};
if (process.argv[2] === '--delete') {
  deleteTours();
} else if (process.argv[2] === '--import') {
  const data = JSON.parse(fs.readFileSync(`${__dirname}/tours-simple.json`, 'utf-8'));
  createTours(data);
}