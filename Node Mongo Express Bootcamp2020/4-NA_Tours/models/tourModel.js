const mongoose = require('mongoose');

const tourSchema = new mongoose.Schema({
  name: {
    type: String,
    required: [true, 'Tour must have name'],
    unique: true,
  },
  duration: {
    type: String,
    required: [true, 'Tour must have duration'],
  },
  maxGroupSize: {
    type: Number,
    required: [true, 'Tour must have group size'],
  },
  difficulty: {
    type: String,
    required: [true, 'Tour must have difficulty'],
  },
  ratingsAverage: {
    type: Number,
    default: 0,
  },
  ratingsQuantity: {
    type: Number,
    default: 0,
  },
  rating: {
    type: Number,
    default: 0.0,
  },
  price: {
    type: Number,
    required: [true, 'Tour must have price'],
  },
  priceDiscount: Number,
  summary: {
    type: String,
    trip: true, //  Trim removes all white spaces beginning and the end of the string
    required: [true, 'Tour must have summary'],
  },
  imageCover: {
    type: String,
    required: [true, 'Tour must have cover image'],
  },
  images: [String], //  Array of strings (paths)
  createdAt: {
    type: Date,
    //  JS date.now() returns mili seconds which are converted to date by mongoose
    default: Date.now(),
  },
  startDates: [Date],

});
//  models always start with Capital letter
const Tour = mongoose.model('Tour', tourSchema);

module.exports = Tour;