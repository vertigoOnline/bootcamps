const Express = require('express');

const app = new Express();

const morgan = require('morgan');

//  morgan  is middleware to log incomming request and include data in the body
//  we should only log it while in dev environement
if (process.env.NODE_ENV === 'development') {
  app.use(morgan('dev'));
}

// custom middlware
// serving static files in web servers
app.use(Express.json());
app.use(Express.static(`${__dirname}/public`));

// routes
const tourRouter = require('./routes/tourRoutes');

app.use('/api/v1/tours', tourRouter);

// eslint-disable-next-line eol-last
module.exports = app;