const fs = require('fs')

const crypto = require('crypto')
const startTime = Date.now()
process.env.UV_THREADPOOL_SIZE = 5 //LUV setting, controling how many thread pools started

setTimeout(() => console.log('Timer #1 finished'), 0);
setImmediate(() => console.log('Immediate #1 finished'))

fs.readFile('./starter/test-file.txt', () => {
  console.log('I/O finished')
  setTimeout(() => console.log('Timer #2 finished'), 0);
  setTimeout(() => console.log('Timer #3 finished'), 3000);
  setImmediate(() => console.log('Immediate #2 finished'))
  //!REMEMBER: setImmediate is executed right after the next tick cycle
  process.nextTick(() => console.log('Process.nextTick'))
  //!REMEMBER: nextTick is executed before next tick

  crypto.pbkdf2('vafe9ain', 'salt', 100000, 1024, 'sha512', () => console.log(Date.now() - startTime + ': password encrypted'))
  crypto.pbkdf2('vafe9ain', 'salt', 100000, 1024, 'sha512', () => console.log(Date.now() - startTime + ': password encrypted'))
  crypto.pbkdf2('vafe9ain', 'salt', 100000, 1024, 'sha512', () => console.log(Date.now() - startTime + ': password encrypted'))
  crypto.pbkdf2('vafe9ain', 'salt', 100000, 1024, 'sha512', () => console.log(Date.now() - startTime + ': password encrypted'))
  crypto.pbkdf2('vafe9ain', 'salt', 100000, 1024, 'sha512', () => console.log(Date.now() - startTime + ': password encrypted'))
  //By default there are 4 thread pools, so encryption of 4 passwords takes around the same time, adding more will result in longer times
  //luckily, we can change the size of pools
})

console.log('Hello from the top level code')