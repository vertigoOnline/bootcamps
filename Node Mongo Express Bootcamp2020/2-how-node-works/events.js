const EventEmitter = require('events')
const http = require('http')

class Sales extends EventEmitter {
  constructor() {
    super()
  }
}

const salesQue = new Sales();

salesQue.on('newSalesOrder', (arg) => console.log(arg))

salesQue.emit('newSalesOrder', {
  order: 123,
  name: 'aaaa'
})

/////////////
const server = http.createServer();
server.on('request', (req, res) => {
  console.log('Request received')
  console.log(req.url)
  res.end('Request received')
})

server.on('request', (req, res) => {
  console.log('Another request 🥶')
  console.log(req.url)
  res.end('Another request 🥶')
})

server.listen(3000, '127.0.0.1', () => console.log('server is up, waiting for requests....'))