//say we need to read large file from customer
const fs = require('fs')
const server = require('http').createServer()

server.on('request', (req, res) => {
  //Solution 1 (takes ages to complete, node can crash as resources can be quickly exceeded)
  // fs.readFile('./starter/test-file.txt', (err, data) => {
  //   if (err) console.log(err)
  //   res.end(data)
  // })

  //Solution 2  (stil problem - readable stream is much faster than sending responce, which can overwelm respomce (back pressure))
  //read from stream, allowing to consume piece by piece. Every time there is new piece of data to consume - stream emmits new event
  // const readable = fs.createReadStream('./starter/test-file.txt')
  // readable.on('data', chunk => {
  //   res.write(chunk) //! Remember: res is writable chunk
  // })
  // readable.on('end', () => res.end())
  // readable.on('error', () => {
  //   res.statusCode = 500
  //   res.end('File not found')
  // })

  //Solution 3 (using Pipe operator, which pipes output. It removes pressure form res, by piping data inot readable stream)
  const readable = fs.createReadStream('./starter/test-file.txt')
  readable.pipe(res)
  //readable source.pipe(writeable destination)
  //in this case res is being piped and written to

})

server.listen(3000, '127.0.0.1', () => console.log('server started...'))