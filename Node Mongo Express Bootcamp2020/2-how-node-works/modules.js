//Option1 - exporting entire class
// const C = require('./test-module')
// const calc = new C()
// console.log(calc.divide(1, 3))

//Option 2 - add properties to exports method
const {
  add,
  multiply,
  divide
} = require('./test-module2')
console.log(add(1, 3))

//caching  (module is loaded once and executed 3 times, behind the scenes it is stored by node)
require('./test-module3')()
require('./test-module3')()
require('./test-module3')()