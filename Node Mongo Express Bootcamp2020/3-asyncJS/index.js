//read the file
const fs = require('fs')
// const http = require('http')
const superagent = require('superagent')

const readFileProm = path => {
  return new Promise((resolve, reject) => {
    fs.readFile(path, (err, data) => {
      if (err) reject(err)
      resolve(data)
    })
  })
}

const writeFileProm = (path, data) => {
  return new Promise((resolve, reject) => {
    fs.writeFile(path, data, err => {
      if (!err) console.log("Random dog saved to file")
    })
  })
}

// Chaining promises - every .then must return promise. This allows to avoid "callback hell"
// readFileProm(`${__dirname}/starter/dog.txt`)
//   .then(data => {
//     return superagent.get(`https://dog.ceo/api/breed/${data}/images/random`)
//   })
//   .then(res => {
//     return writeFileProm('dog-img.txt', res.body.message)
//   })
//   .then(() => console.log("Random dog image saved to file"))
//   .catch(err => console.log(err))


/// ASYNC/AWAIT by far is the most leanest way chaining 

const getDogPictAsync = async () => {
  try {
    const data = await readFileProm(`${__dirname}/starter/dog2.txt`)
    console.log(`Breeed: ${data}`)
    const res = await superagent.get(`https://dog.ceo/api/breed/${data}/images/random`)
    console.log(res.body.message)
    writeFileProm('dog-img.txt', res.body.message)
  } catch (error) {
    console.log(console.log(error))
    throw error
  }
  return '2: Ready 🐶'
}

(async () => {
  try {
    console.log('1 Will get the dog picts')
    const x = await getDogPictAsync()
    console.log(x)
    console.log('3: Done getting dog pict')
  } catch (error) {
    console.log('error !!!  💥')
    throw console.error(error);
  }
})()