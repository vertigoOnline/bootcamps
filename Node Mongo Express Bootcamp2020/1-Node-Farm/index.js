// Building WEB SERVER
const http = require('http')
const fs = require('fs')
const url = require('url')

const replaceTemplate = require('./modules/replaceTemplate')

const data = fs.readFileSync(`${__dirname}/dev-data/data.json`, 'utf-8')
const producData = JSON.parse(data)

const tempOverview = fs.readFileSync(`${__dirname}/templates/template-overview.html`, 'utf-8')
const tempCard = fs.readFileSync(`${__dirname}/templates/template-card.html`, `utf-8`)
const tempProduct = fs.readFileSync(`${__dirname}/templates/template-product.html`, `utf-8`)

const server = http.createServer((req, res) => {
  const {
    query,
    pathname
  } = url.parse(req.url, true)
  // parse is a built in function which allows to break down query ie, products?id=1
  // firstly we need to get req.url, and then parse it through. 
  //use true flag to  parse into query object

  //Product page
  if (pathname == '/product') {
    res.writeHead(200, {
      'Content-type': 'text/html'
    })
    const product = producData[query.id]
    const productHTML = replaceTemplate(tempProduct, product)
    res.end(productHTML)

    //Overview page
  } else if (pathname == '/overview') {
    res.writeHead(200, {
      'Content-type': 'text/html'
    })
    const cardsHtml = producData.map(el => replaceTemplate(tempCard, el)).join('')
    //join is required since map returns array and we need one joined string for html

    const output = tempOverview.replace('{%PRODUCT_CARDS%}', cardsHtml)
    res.end(output)

    //Home page
  } else if (pathname === '/') {
    res.end('welcome to the homemage')

    //API
  } else if (pathname === '/API') {
    res.writeHead(200, {
      'Content-type': 'application/json'
    })
    res.end(data)

    //NOT FOUND
  } else {
    res.writeHead(404, {
      'Content-type': 'text/html',
      'my-own-header': 'hello header'
    })
    res.end('<h1>This page does not exist</h1>')
  }
})

server.listen(3000, '127.0.0.1', () => {
  console.log('server started')
})


//###############################################################
//WORKING WITH FILES
// const fs = require('fs')
//fs stands for 'file system'
// fs.readFile('./txt/start.txt', 'utf-8', (err, data) => {
//   fs.readFile(`./txt/${data}.txt`, 'utf-8', (err, data2) => {
//     console.log(data2)
//     fs.readFile('./txt/append.txt', 'utf-8', (err, data3) => {
//       console.log(data3)
//       fs.writeFile('./txt/final.txt', `${data2}\n${data3}`, 'utf-8', err => {
//         console.log('file has been written 🌟')
//       })
//     })
//   })
// })
// console.log('will read file')