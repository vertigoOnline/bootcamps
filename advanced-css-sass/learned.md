# Image clip
[ ] background-image: url('');	-	add mage into the container
[ ] background-size: cover;		- 	fits image in the frame of container
[ ] background-position: top;	-	as you resize window - it fixes image (top, bottom or any) and crops the other size to fit into the window view
[ ] padding is not inherited
[ ] clip-path: polygon (x y, x y, x y)			- 	clips image

* Gradients
[ ] can set gradient via `background-image: linear-gradient(angle, color1, at%, color2 at %)
[ ] can be either simple linear-gradient(to right bottom, red, green)
[ ] can be more specific like linear-gradient(105deg, red 0%, red 50%, green 50%) This is called solid gadient with *sharp line*

* Building header element, Center anything with transform, left and top
[ ]	For header we use 95vh which allows us to occupie screen by 95 %
[ ] For logo and elements which have to appear in the middle (or top corner) of header we use position: absolute (note parent has to be set relateive)
[ ] Absolute removes element from the document flow. Use top, left to position element where needed
[ ] Element which is being centered need to use transform: translate(-50%, -50%) since positioning works for top left corner of element. You need this point moved to the center of element
[ ]	Animation: use keyframes <animation name=""> { 0%{} 100%{}} with 2 paramteres, normally opacity and transform: translate()  which lets to do. (2 due to performance reasons)
[ ] Centering: if element displayed inline (or inline block) - cancenter it simply by applying text-aling: center property  parent element
[ ] transform: translateX - only works if element is displayed as block or inline block

* Pseudo classes, button animation
[ ] animation-fill-mode: backwards; - applies styles of  animation at 0% before animation (prevents el entering the scene to be .visible before animation starts )

# How CSS works

* Specificity
[ ] Inline style > ID > Classes, pseudo classes, attribute > Elements, pseudo elements

* Value processing
[ ] Each prop has initial val even if its not declared
[ ] root font size 16px
[ ] % and relative units always converted to px
[ ] % for font-sizes is relative to its parent font-size
[ ] % for lengths is relative to its parent width
[ ] em for font-sizes is relative to its parent
[ ] em for width is relative to current font
[ ] rem is always in relation to root font-size (default is 16 px, user-agent defined)
[ ] vh and vw relates to viewport

* Inheritance
[ ] Props related to fonts are inherited
[ ] Margins, paddings etc are not inherited
[ ] its Computed value which gets passed - not declared
[ ] Only works on one certain property
[ ] Keyword: inherit forces inheritance on property
[ ] Keyword: reset forses reset on its initial value

* Using relative units
  [ ] Great practice is to use rem units since it enables  easy breakpoints for mobile devices
  [ ] rem units are in relation to *root* element font size which is set on *html element*
  [ ] we set font size in *root element to 10px* then all other relative rem'swill be at *1 rem = 10px*
  [ ] *problem* with hardcoding px in the root element is it overwrites user default font size (which might be different to  *default 16px*)
  [ ] *solution* to this is to use *%*. 100% is equal to the browser font size settig (default 16). Since we need 1rem=10px - we need 62.5% of 16px), so *root font size shouldbe equal to 62.5% if we aim to have 1rem = 10px ratio*
  [ ] Try to use inheritance as much as possible (example box sizing: border box is set on HTML, then * elements - inherit)
* BEM (Block__Element--Modifier)
  [ ] Block should be unique
  [ ] If element can leave outside of block  - it could very much be separate (not part of __ )
* SCSS
  [ ] scss paths (images etc) *should be relative to compiled css file*. Not sccs
  [ ] @mixin() brings in placeholder code (can accept props)
  [ ] @extends (moves block to extends declaration (opposite of @mixin() )) *try to use it only when elements related*
  [ ] @function()
  [ ] When doing math operations *1px will conver to px

[ ] 7:1 scss architecture
[ ] use _partials

* Creating layout using floats
  [ ] it is number of rows and cols on the page
  [ ] *row* takes up *max-width* of 1140px (converted to rem this is 114rem assuming root font size is at 62.5% of default 16 (10px = rem))
  [ ] cols then float left and take up *calc* 'ulated width
  [ ] its important to apply *::after* el. reset on row otherwise row box will clash when el;s floated left. this is done via  **clearfix*
  `row::after{ content: ""; clear: both; display: table; }`

* Building page
  [ ] The content inside the <main> element should be unique to the document. It should not contain any content that is repeated across documents such as sidebars, navigation links, copyright information, site logos, and search forms.

* Working with images
  [ ] Alwats aim to use % on image width, in this case images become responsive
  [ ] Outline is similar to border, but it allows us to use `offset: `, creating padding space between image and the border
  [ ] Image placed in the card will soften edges (border radius). `overlay: hidden` will stop this

* Using position relative and fixed
  [ ] Parent element must be set to `relative`
  [ ] Children set to `absolute`
  [ ] Note that *All childer will then collapse* to each other - so *width* has to be set  to 100%
  [ ] As a result, parent will also collapse so width has to be set to original *rems*
  [ ] 

* Flipping cards effect
  [ ] This is atchieved through `transform rotate` properties
  [ ] Creating 2 sides of the card and rotating one and another
  [ ] `backface-visibility: hidden`
  [ ] if I use `transition: All ` - by default it applies linear animation. 
  [ ] For this effect, its better to use `ease`
  [ ] To put cards on top each other use `relative` & `absolute` positioning
  [ ] Image inside card overflows border and sharpens edges. To stop this use `overflow: hidden` property
  [ ] 
  
* Text around the circle shape effect 
  [ ] For this to work , container element must float and have specified width and height
  [ ] `shape-outside: circle(radius % at, 50% 50%)`. Latter specifies that it start exactly at the center
  
* Using videos in HTML
  [ ] Creating <video> tag, `autoplay`, `muted`, `loop`
  [ ] Video placed behind container using `position: relative` and `position: anbsolute`
  [ ] Video should fit into contiainer maintaining origina aspect ratio using `object-fit:cover` (remember to set video width and heigh to 100)
  [ ] To avoid Video cutting corners etc - use `overflow: hidden`

* Creating forms, custom radio buttons



* Powerfull selectors
  [ ] `:not(:last-child)` - applies to all except last
  [ ] &_input`:placeholder-shown + `&__label {}  - selects immediate sibling when form input placeholder is shown
  [ ] `-webkit-input-placeholder` {} styles input placeholder
  [ ] `:: invalid   :: valid` - styles form element on valid/invalid input 


* Extra topics

  [ ] Checkbox hack 
  [ ] Transform origin
  [ ] Animation timing functions using `cubic bezier curves`
  [ ] Animate solid-color gradients