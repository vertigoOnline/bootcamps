/*
* Primary file for the API
*
*
*/
 
//Dependencies
// 1) Built in node server
const http = require('http') 

// 2) Built in URL parser
const url = require('url')

// 3) Spring decoder for parsing payload
const { StringDecoder } = require('string_decoder');
const { stat } = require('fs');

// Server should respond to all requests with string
const server = http.createServer((req,res) => {

// ######## PROCESS REQUEST ########
    //A) Get URL and parse it. it creates URL object with pathname and arguments. "True" means to pass querystring module 
    const parsedUrl =  url.parse(req.url, true); 

    //B) Get the path
    const path = parsedUrl.pathname
    // remove unwanted slashes localhost /foo/bbd/ or /foo/bbd will return the same
    const trimmedPath  = path.replace(/^\/+|\/+$/g,'')
    console.log('Request received on path: ' + trimmedPath);
    
    //C) Get the query string as an object 
    const queryStringObject = parsedUrl.query;
    
    //D) Understand what HTTP method is used
    const httpMethod = req.method.toLowerCase();
    
    //E) Get the headers as an object
    const headers = req.headers
    
    //F) Get the payload if any
    // Payloads come in to http server as stream of data. 
    // Hence we use decoder to collect pieces and return full payload once its finished
    let decoder = new StringDecoder('utf-8');
    let buffer = '';
    // lisen to an 'data' event , decode and add it to the buffer
    req.on('data', (data) =>{
        buffer += decoder.write(data)
    })
    //once finished, end decoder and send buffer
    req.on('end', ()=>{
        buffer += decoder.end();
        //Log everything
        // console.log(`HTTP request type is ${ httpMethod } with the following query paramters: `);
        // console.log(queryStringObject);
        // console.log('Request received with following headers:', headers);
        // console.log('Payload:', buffer);

        const chosenHandler = trimmedPath in handlers ? handlers[trimmedPath] : handlers['notfound']
              
        //construct data object (to pass to handler)
        const data = {
            'trimmedPath': trimmedPath,
            'queryStringObject' : queryStringObject,
            'method' : httpMethod, 
            'headers': headers,
            'payload': buffer
        }
        //route request to the hanlder 
        chosenHandler(data, (statusCode, payload)=>{
            //Use status code called back the handler or default to 200
            statusCode = typeof(statusCode) == 'number' ? statusCode : 200
            
            //Use payload callback by the handler or default to empty object
            payload = typeof(payload) == 'object' ? payload : {}

            //convert to string
            const payloadString = JSON.stringify(payload)

            //return the response
            //set status head for response
            res.setHeader('Content-Type', 'application/json')
            res.writeHead(statusCode)
            res.end(payloadString)

            console.log('server returning this response: ', statusCode, payloadString);
        })

    })
    // Send the response

})
 

// Start server, lisen on port 3000
const port = process.env.PORT || 3000
server.listen(port,  ()=>{
    console.log('Server is lisening on port: ' + port);
})

// Define handlers
const handlers = {};
    //Sample handler
    handlers.sample =  (data, callback) => {
    //callback http status code, and a payload object
        callback(406, {'name' : 'response from sample handler route'})
    }; 
    //Not found handlers
    handlers.notfound = (data, callback) => {
        callback(404)
    };
// Define a request router
const router = {
    'sample': handlers.sample
};