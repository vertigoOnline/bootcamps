const express = require('express');
const connectToDB = require('./config/db')

const app = express();

// Connect DB
connectToDB()


// Init middleware 
    // Enables body parser
    app.use(express.json({extended: false})) 

app.get('/', (req,res)=>{
    res.send('API is running')

})

// Setup routes
app.use('/api/users', require('./routes/api/users'));
app.use('/api/auth', require('./routes/api/auth'));
app.use('/api/profile', require('./routes/api/profile'));
app.use('/api/posts', require('./routes/api/posts'));

//once this app is deployed to heroku it will pick up PORT variable from process.env, until then we want to use port 5000 locally
const PORT = process.env.PORT ||  8000; 
app.listen(PORT, ()=>{ 
    console.log(`Server started on port: ${ PORT } `)
})

