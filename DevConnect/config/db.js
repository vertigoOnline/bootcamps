const mongoose = require ('mongoose');
const config = require('config');



const connectToDB = async () => {
    try {
        //TODO:  move passowrd in clear to .ENV
        await mongoose.connect(config.get('mongoURI'), { useNewUrlParser: true, useUnifiedTopology: true , useCreateIndex: true })
        console.log('Connected to databse');
    }
    catch(error) {
        console.log('error connecting to DB: ' + error)
        // exit process with failure 
        process.exit(1);
    }
}
module.exports = connectToDB

