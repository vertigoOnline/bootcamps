const express = require('express');
const router = express.Router();
const auth = require('../../middleware/auth');
const {check, validationResult} = require('express-validator');
const User = require('../../models/User');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');
const config = require('config')
// @route       GET api/auth
// @desc        Auth route. validates token  and returns user information 
// @access      Public 
router.get('/', auth, async (req,res)=>{
    try {
         const user = await User.findById(req.user.id).select('-password')
         res.json(user)
    } catch (error) {
        console.log(error.message)
        res.status(500).json({ msg: 'Server Error'})   
    }
})

// @route       POST api/auth
// @desc        Logs user in (validates password etc)
// @access      Public 

router.post('/', 
    [   check('email', 'Email is required').isEmail(),
        check('password', 'Password is required').exists()]

    , async (req,res) =>{
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            return res.status(400).json({ errors : errors.array()})
        }
        else {
            const { email, password} = req.body
            try {
                let user = await User.findOne({ email: email })
                if(!user) {
                    return res.status(400).json( { errors: [{msg: 'Invalid credentials'}] })
                }
                
                const doesPasswordMatch = await bcrypt.compare(password, user.password)
                if (!doesPasswordMatch){
                    return res.status(400).json({ errors: [{msg: 'Invalid credentials'}] })
                }

                const payload = {
                    user: {
                        id: user.id
                    }
                };
                
                jwt.sign(
                    payload, 
                    config.get('jwtSecret'),
                    { expiresIn: 360000},
                    (err, token) => {
                       if(err) throw err;
                       res.json({ token }); 
                    }
                )

            } catch (error) {  
                console.log(error.message)
                return res.status(500).json('server error')
            }

        }
    }
)

module.exports = router;