const express = require('express');
const router = express.Router();
const auth = require('../../middleware/auth');
const {check, param, validationResult} = require('express-validator');

const Profile = require('../../models/Profile');
const User = require('../../models/User');
const { remove } = require('../../models/Profile');
const request = require('request');
const config = require('config')

// @route       GET api/profile/me
// @desc        Gets current users profile 
// @access      Private 
router.get('/me', auth, async (req,res)=>{
try {
    // remember that auth middlewhare (if user is authentified) injects token which has  user.Id so we have access to it    
    const profile = await Profile.findOne({user: req.user.id}).populate('user', ['name', 'avatar'])
    if(!profile){
        return res.status(400).json({msg: 'There is no profile for this user'})
    }
    res.json(profile)
}
catch(err) {
    console.log(err);
    res.status(500).send({errors: {msg: 'Server Error'}});
}
})

// @route       POST api/profile
// @desc        Create or update user profile 
// @access      Private 
router.post('/', 
    [
        auth, 
        [
            check('status', 'Status is required').not().isEmpty(),
            check('skills', 'Skills is required').not().isEmpty(),
        ]
    ],
    async (req, res)=>{
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            return res.status(400).json({errors: errors.array()});
        }
       const {
            company, 
            website,
            location,
            bio, 
            status, 
            githubusername, 
            skills, 
            youtube, 
            facebook, 
            twitter, 
            instagram,
            linkedin
        } = req.body

        // build profile object
        const profileFields = { user: req.user.id }
        if(company) profileFields.company = company;
        if(website) profileFields.website = website;
        if(location) profileFields.location = location;
        if(bio) profileFields.bio = bio;
        if(status) profileFields.status = status;
        if(githubusername) profileFields.githubusername = githubusername; 
        if(skills) {
            profileFields.skills = skills.split(',').map(skill => skill.trim());
        }
        // build social obeject
        profileFields.social = {}
        if(youtube) profileFields.social.youtube = youtube;
        if(twitter) profileFields.social.twitter = twitter;
        if(instagram) profileFields.social.instagram = instagram;
        if(linkedin) profileFields.social.linkedin = linkedin;

        try {
            let profile = await Profile.findOne({ user: req.user.id })
            if(profile) {
                //update
                profile = await Profile.findByIdAndUpdate(
                    { user: req.user.id},
                    { $set: profileFields},
                    { new: true}
                    )          
                return res.json(profile)  
            }        
            else {
                //create 
                profile = new Profile(profileFields);
                await profile.save();
                return res.json(profile);
            }
        } catch (err) {
            console.error(err.message)
            res.send(500).send('Server Error at profile creation')
        }
    }
)

// @route       GET api/profile
// @desc        Get all profiles 
// @access      Public 

router.get('/', async (req,res) => {
    try {
        let profiles = await Profile.find().populate('user', ['name', 'avatar']);
        console.log(profiles);
        return res.status(200).json(profiles);

    } catch (err) {
        console.error(err)
        res.status(500).send({errors: {msg: "server error getting all profiles"}})
    }
})

// @route       PUT api/profile/experience
// @desc        Add experience to the profile
// @access      private

router.put('/experience', 
    [auth, 
    check('title', 'Title is required').not().isEmpty(),
    check('company', 'Company is required').not().isEmpty(),
    check('from', 'from is required').not().isEmpty(),
], async (req,res) =>{
    const errors  = validationResult(req);
    if(!errors.isEmpty()) {
       return res.status(400).json({errors: errors.array()})
    }
    const {title, company, location, from, to, current, description} = req.body;
    const newExperience = {title, company, location, from, to, current, description};
    try {
        const profile = await Profile.findOne({user: req.user.id})
        if(!profile){
            return res.status(500).send({msg: 'error, profile not found for the user'})
        }

        profile.experience.unshift(newExperience); //push new experience to the start of array
        await profile.save()
        res.status(200).send(profile)

    } catch (err) {
        console.error(err)
        res.status(500).send('Server error on experience creation')
    }
})

// @route       DELETE api/profile/experience
// @desc        Remove experience to the profile
// @access      private

router.delete('/experience/:exp_id', auth, async (req,res)=>{
    try {
        const profile = await Profile.findOne({ user: req.user.id })
        //find index to remove by going through ids and finding which index in array needs to be removed
        const removeIndex = profile.experience.map(item => item.id).indexOf(req.params.exp_id);
        if(removeIndex > -1) {
            profile.experience.splice(removeIndex,1);
            await profile.save()               
        }     
        res.status(200).send(profile)
    } catch (err) {
        console.error(err)
        res.status(500).send({msg: 'Server error on experience deletion'})
    }
})


// @route       PUT api/profile/education
// @desc        Add education to the profile
// @access      private

router.put('/education', 
    [auth, 
    check('school', 'School is required').not().isEmpty(),
    check('degree', 'degree is required').not().isEmpty(),
    check('fieldofstudy', 'Field of study is required').not().isEmpty(),
    check('from', 'from is required').not().isEmpty(),
], async (req,res) =>{
    const errors  = validationResult(req);
    if(!errors.isEmpty()) {
        return res.status(400).json({errors: errors.array()})
    }
    const {school, degree, fieldofstudy, from, to, current, description} = req.body;
    const newEducation = {school, degree, fieldofstudy, from, to, current, description};
    try {
        const profile = await Profile.findOne({user: req.user.id})
        if(!profile){
            return res.status(500).send({msg: 'error, profile not found for the user'})
        }

        profile.education.unshift(newEducation); //push new experience to the start of array
        await profile.save()
        res.status(200).send(profile)

    } catch (err) {
        console.error(err)
        res.status(500).send('Server error on education creation')
    }
})

// @route       DELETE api/profile/education
// @desc        Remove education to the profile
// @access      private

router.delete('/education/:educ_id', auth, async (req,res)=>{
    try {
        const profile = await Profile.findOne({ user: req.user.id })
        //find index to remove by going through ids and finding which index in array needs to be removed
        const removeIndex = profile.education.map(item => item.id).indexOf(req.params.educ_id);
        if(removeIndex > -1) {
            profile.education.splice(removeIndex,1);
            await profile.save()                 
        }
        
        res.status(200).send(profile)
    } catch (err) {
        console.error(err)
        res.status(500).send({msg: 'Server error on education deletion'})
    }
})

// @route       Get api/profile/github/:username
// @desc        Get repositories from github
// @access      Public

router.get('/github/:username', async (req,res) =>{
    try {
        const options = {
            uri: `https://api.github.com/users/${req.params.username}/repos?per_page=5&sort=created:asc&client_id=${config.get('githubClientId')}&client_secret=${config.get('githubClientSecret')}`,
            method: 'GET',
            headers: {'user-agent' : 'node.js'}
        }
        request(options, (err, resp, body)=>{
            if(err) console.log(err);
            if(resp.statusCode !== 200) {
                return res.status(404).json({msg: "no Github profile found"})
            }
            res.json(JSON.parse(body))
        })
    } catch (err) {
        console.error(err)
        res.status(500).send({ mgs: 'error fetching github profile' })

    }
})
module.exports = router;