const express = require('express');
const router = express.Router();
const { check, validationResult } = require('express-validator');
const User = require('../../models/User');
const Profile = require('../../models/Profile');
const gravatar = require('gravatar');
const bcrypt = require('bcryptjs');
const jwt = require ('jsonwebtoken');
const config = require('config')
const auth = require('../../middleware/auth')

// @route       POST api/users
// @desc        Register new user
// @access      Public 
router.post('/', 
  [ 
      check('name', 'Name is required').not().isEmpty(),
      check('email', 'Email is required').not().isEmpty(),
      check('password', 'Password is required').isLength({min: 6})
  ],
  async (req,res)=>{
    const errors = validationResult(req);
    if(!errors.isEmpty()){
        return res.status(400).json({errors: errors.array()});     
    }

    const {name, email, password} = req.body
    try {      
        // See if user already exists
        let user = await User.findOne({ email: email})
        if (user) {
            console.log('user already exists, responding 400');
            res.status(400).json({errors: [ { msg: 'User already exists' }]});
        }
        else {
            // Get users gravatar (takes email and additional parameters)
            const avatar = gravatar.url(email, {
                s: '200',
                r: 'pg',
                d: 'mm'
            });
            user = new User({
                name, 
                email, 
                password, 
                avatar
            })
            // Encrypt pwd using bcrypt
            const salt = await bcrypt.genSalt(10);
            user.password = await bcrypt.hash(password, salt);
            
            // Save user to DB
            await user.save();
        
            // Return jsonwebtoken          
            const payload = {
                user: {
                    id: user.id
                }
            }
            jwt.sign( 
                payload, 
                config.get('jwtSecret'), 
                {expiresIn: 360000 },
                (err, token) =>{
                    if(err) throw err;
                    res.json({ token })   
                }
            )

        }
    } catch (err) {
        // console.error(err.message)
        res.status(500).send('Server error, validating user failed')
    }
})

// @route       GET api/users/{id}
// @desc        profile by user id
// @access      Public 
router.get('/:user_id',  async (req,res) =>{
    try {
        const profile = await Profile.findOne({ user: req.params.user_id }).populate('user', ['name', 'avatar']);
        if (profile) {
            console.log('profile found'+ profile);
            res.status(200).send(profile);
        }
        else{
            res.status(401).send({msg: "No profile for this user"})
        }

    } catch (err) {
        if(err.kind == 'ObjectId') {
            return res.status(401).send({errors: {msg: "No profile for this user"}})
        } else {
            return res.status(500).send({errors: {msg: "Internal server error trying to get user profile information"}})
        }
    }
})


// @route       DELETE api/users/{id}
// @desc        profile by user id
// @access      Private 

router.delete('/', auth, async (req,res) => {
    await Profile.findOneAndRemove({ user: req.user.id });
    await User.findOneAndRemove({ _id: req.user.id });
    res.status(200).json({msg: 'user deleted'});
})


module.exports = router;