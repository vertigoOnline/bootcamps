const express = require('express');
const router = express.Router();
const auth = require('../../middleware/auth');
const {check, validationResult} = require('express-validator');
const User = require('../../models/User');
const Profile = require('../../models/Profile');
const Post = require('../../models/Post');


// @route       POST api/posts
// @desc        Create post
// @access      private 
router.post('/', [
    auth, 
    check('text', 'Text must is required').not().isEmpty()],
    
    async (req,res)=>{
    try {
        const errors =  validationResult(req); 
        if(!errors.isEmpty()) {
            return res.status(400).send({errors: errors.array()})
        }
        const user = await User.findById(req.user.id).select('-password');
        const newPost = {
            user: user.id,
            name: user.name,
            avatar: user.avatar,
            text: req.body.text,
        }
        
        const post =  new Post(newPost);
        await post.save();
        return res.status(200).send(post);

    } catch (err) {
        console.error(err)
        return res.status(500).send('Server error creating post')        
    }
})

// @route       GET api/posts
// @desc        Get all the posts
// @access      private 
router.get('/', auth, async (req,res)=>{
    try {
        const posts = await Post.find().sort({date: -1 })
        res.status(200).send(posts)
    } catch (err) {
        console.error(err)
        res.status(500).send({error: 'Server error getting all posts '})
    }
})


// @route       GET api/posts/:post_id
// @desc        Get post by ID
// @access      private 
router.get('/:post_id', auth, async (req,res) =>{
    try {         
        const post = await Post.findById(req.params.post_id)
        if(!post) { 
            return res.status(404).send({msg: 'Post not found'})
        }
        return res.status(200).send(post);
    } catch (err) {
        console.error(err.message)
        if(err.kid = 'ObjectId') {
            return res.status(404).send({msg: 'Post not found'})
        }
        else return res.status(500).send({error: 'Server error getting post by ID '})
    }
})

// @route       DELETE api/posts/:post_id
// @desc        Delete post by ID
// @access      private
router.delete('/:post_id', auth, async (req,res) =>{
    try {         
        //find post
        const post = await Post.findById(req.params.post_id)
        if(!post) { 
            return res.status(404).send({msg: 'Post not found'})
        }
        //check if user can delete it 
        if(post.user.toString() !== req.user.id) {
            return res.status(401).send({msg: 'Not authorised to remove post'})
        }
        else {
            await post.remove();
            return res.status(200).send({msg: "post removed"});
        }
    } catch (err) {
        console.error(err.message)
        if(err.kid = 'ObjectId') {
            return res.status(404).send({msg: 'Post not found'})
        }
        else return res.status(500).send({error: 'Server error deleting post by ID '})
    }
})

// @route       PUT api/posts/like/:id
// @desc        update post, like
// @access      private
router.put('/like/:post_id', auth, async (req,res)=>{
    try {
       const post = await Post.findById(req.params.post_id);
       //check if post already been liked by user      
       if(post.likes.filter((x)=> x.user.toString() === req.user.id).length > 0 ) {
           return res.status(400).json({msg: 'Post already liked'});
       }
       post.likes.unshift({user: req.user.id})
       await post.save()
       res.status(200).send(post.likes)
    } catch (err) {
        console.error(err.message);
        res.status(500).send({error: 'Server error adding like to the post'})
    }
})

// @route       PUT api/posts/unlike/:id
// @desc        update post, unlike
// @access      private
router.put('/unlike/:post_id', auth, async (req,res)=>{
    try {
       const post = await Post.findById(req.params.post_id);
       //check if post already been liked by user      
       if(post.likes.filter((x)=> x.user.toString() === req.user.id).length == 0 ) {
            return res.status(400).json({msg: 'Post has not yet been liked'});
       }
    
       //get remove index 
       const removeIndex = post.likes.map(like=>like.user.toString()).indexOf(req.user.id)
       post.likes.splice(removeIndex,1);
       await post.save()
       res.status(200).send(post.likes)
    } catch (err) {
        console.error(err.message);
        res.status(500).send({error: 'Server error adding like to the post'})
    }
})

// @route       Post api/posts/comment
// @desc        update post, comment
// @access      private
router.post('/comment/:post_id', [
    auth, 
    check('text', 'Text must is required').not().isEmpty()],
    
    async (req,res)=>{
    try {
        const errors =  validationResult(req); 
        if(!errors.isEmpty()) {
            return res.status(400).send({errors: errors.array()})
        }
        const user = await User.findById(req.user.id).select('-password');
        const post = await Post.findById(req.params.post_id);

        const newComment = {
            user: user.id,
            name: user.name,
            avatar: user.avatar,
            text: req.body.text,
        }
        
        post.comments.unshift(newComment)
        await post.save();
        return res.status(200).send(post);

    } catch (err) {
        console.error(err.message)
        return res.status(500).send('Server error creating post')        
    }
})

// @route       Post api/posts/comment
// @desc        update post, comment
// @access      private
router.delete('/comment/:post_id/:comment_id', 
    auth,    
    async (req,res)=>{
    try {
       
        const user = await User.findById(req.user.id).select('-password');
        const post = await Post.findById(req.params.post_id);
        
        //pull out comment
        const comment = post.comments.find(comment =>  comment.id === req.params.comment_id);
        if(!comment) {
            return res.status(404).send({msg: 'not found'})
        }        
        else {
            //check user
            if (comment.user.toString() !== req.user.id) {
                return res.status(401).send({msg: 'Not authorised'})
            }
        }
        // get remove index
        const indexDelete = post.comments.map(user=>user.toString()).indexOf(req.user.id)
        post.comments.splice(indexDelete,1);
        await post.save();
        return res.status(200).send(post);

    } catch (err) {
        console.error(err.message)
        return res.status(500).send('Server error deleteing comment')        
    }
})

module.exports = router;