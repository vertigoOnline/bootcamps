# CLIENT package dependencies
* axios          calling apis
* redux-thunk    middleware making async requests  
* moment         date & time library

# SERVER package dependencies
* concurently    alows to run npm scripts simultainiously (dev and server)
* config         save configuration use everywhere
* request        making api calls
* bcrypt         for hashing passwords
* express-valid  for validating requests
* jsonwebtoken   for handling jwt token