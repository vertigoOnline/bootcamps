const jwt = require('jsonwebtoken');
const config = require('config');

module.exports = function (req,res,next) {
    // Get token from the header
    token = req.header('x-auth-token');
    // Validate token
    if(!token) {
        return res.status(401).json({ msg: 'No token, authorization denied'});
    }
    try {
        const decoded = jwt.verify(token, config.get('jwtSecret'))
        //JWT token contains user ID  (if valid) I need to extract it
        req.user = decoded.user;
        next();
    } catch (error) {
        res.status(401).json({ msg: 'Invalid token'})
    }
}
