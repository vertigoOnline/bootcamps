import {SET_ALERT, REMOVE_ALERT} from './types';
import  uuid from 'uuid/v4';
//want to be able to dispatch more than 1 
//dispatch is a middleware which allows us to use this
//uuid will generate 
export const setAlert = (msg, alertType) => dispatch => {
    const id = uuid(); //random long string
    dispatch({
        type: SET_ALERT,
        payload: {msg, alertType, id}
    });

    //clear error after some time
    setTimeout(() => dispatch({ type: REMOVE_ALERT, payload:id}), 2000);
}
