//takes state and action
//action will be dispateched from actions file
import {SET_ALERT, REMOVE_ALERT} from '../actions/types'

const initialState = []

export default function alertAction (state = initialState, action) {
    const {type, payload} = action
    switch (type) {
        case SET_ALERT:
            //state is immutable so we copy
            //payload is payload we send
            return [...state, payload];
        case REMOVE_ALERT:
            return state.filter(alert=>alert.id !== payload)
        default:
            return state
    }
}