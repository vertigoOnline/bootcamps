import React, { Fragment, useState } from 'react';
import {Link} from 'react-router-dom';


import { connect } from 'react-redux';
import { setAlert} from '../../actions/alert';

//allows to check prop types . TS is not helpfull for props since its for variable declarations
import PropTypes from 'prop-types'; 

const  Register = (props) => {
    const [formData, setFormData] = useState({
        name: '',
        email: '',
        password: '',
        password2: '',
    });
    
    const {name, email, password, password2 } = formData;
    // sets state by copying object (spread operator) and changing field value. picksup field name from html and updates its value in the state.
    const handleInput = e => setFormData({ ...formData, [e.target.name]: e.target.value })

    const handleSubmit = async e => {
        e.preventDefault();
        if(password !== password2 ) {
            props.setAlert('Passwords do not match', 'danger')
        }
        else {
          console.log('success')
        
          }  
    }

    return (
    <Fragment>
      <h1 className="large text-primary">Sign Up</h1>
      <p className="lead"><i className="fas fa-user"></i> Create Your Account</p>
      <form className="form" onSubmit={e=>handleSubmit(e)}>
        <div className="form-group">
          <input type="text" placeholder="Name" name="name" value={ name } onChange = {e=>handleInput(e)} required />
        </div>
        <div className="form-group">
          <input type="email" placeholder="Email Address" value = {email} onChange= {e=>handleInput(e)} name="email" />
          <small className="form-text"
            >This site uses Gravatar so if you want a profile image, use a
            Gravatar email</small
          >
        </div>
        <div className="form-group">
          <input
            type="password"
            placeholder="Password"
            name="password"
            minLength="6"
            value = { password}
            onChange = {e=>handleInput(e)}
          />
        </div>
        <div className="form-group">
          <input
            type="password"
            placeholder="Confirm Password"
            name="password2"
            minLength="6"
            value = {password2}
            onChange = {e=>handleInput(e)}
          />
        </div>
        <input type="submit" className="btn btn-primary" value="Register" />
      </form>
      <p className="my-1">
        Already have an account? <Link to="/login">Sign In</Link>  
      </p>

    </Fragment>
    )
}

Register.propTypes = {
  setAlert: PropTypes.func.isRequired
}

/* connect ( 
   state  - state we want to pass to component
   action obect - it will allow to access props.setAlert ,  
*/

export default connect(null, { setAlert })(Register);